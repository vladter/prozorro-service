const Tenders = require('./libs/mongoose');
const request = require('request');

const link = `http://public.api.openprocurement.org/api/2.4/tenders?offset=${new Date().toISOString()}+02.00`;

function updateData(url) {
  return request(url, { json: true }, (error, response, body) => {
    if (error) {
      console.log(error);
    }
    saveDataToDb(body);

    if (body.next_page) {
      updateData(body.next_page.uri);
    }
  });
}

function saveDataToDb(body) {
  const tendersList = [];
  let tendersData = new Tenders({ tenderId: { type: String, index: true }, tenderDate: String });

  body.data.forEach((item) => {
    tendersData = {
      tenderId: item.id,
      tenderDate: item.dateModified,
    };
    tendersList.push(tendersData);
  });

  tendersList.forEach((item) => {
    Tenders.update(
      {
        tenderId: item.tenderId,
        tenderDate: item.tenderDate,
      },
      { $set: item },
      { upsert: true, multi: true },
      (err) => {
        if (err) console.log(err);
      },
    );
  });
  console.log('Data saved to db');
}

updateData(link);

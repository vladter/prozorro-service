const mongoose = require('mongoose');
const config = require('../config');

mongoose.Promise = global.Promise;
mongoose.connect(
  config.get('mongoose:uri'),
  { useMongoClient: true },
);

const tender = new mongoose.Schema({
  tenderId: { type: String, index: true },
  tenderDate: String,
});

const Tenders = mongoose.model('Tenders', tender);

module.exports = Tenders;
